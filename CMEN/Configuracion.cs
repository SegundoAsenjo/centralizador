﻿using System;
using System.Configuration;

namespace CMEN
{
    public static class Configuracion
    {
        public static string UsuarioServicio
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings.Get("UsuarioServicio"));
            }
        }
        public static string PasswordServicio
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings.Get("PasswordServicio"));
            }
        }
    }
}