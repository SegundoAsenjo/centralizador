﻿

function strPad(i, l, s) {
    var o = i.toString();
    if (!s) { s = '0'; }
    while (o.length < l) {
        o = s + o;
    }
    return o;
}

function parseJsonDate(jsonDate) {
    if (jsonDate == null)
        return '';
    var value = new Date(parseInt(jsonDate.substr(6)));
    var ret = strPad(value.getDate(), 2) + "/" + strPad(value.getMonth() + 1, 2) + "/" + value.getFullYear();
    return ret;
}

var CMEN = {
    languageDatatable: {
        "decimal": ".",
        "lengthMenu": "Mostrar _MENU_ registros por página",
        "zeroRecords": "No se encontraron registros.",
        "info": "Pág. _PAGE_ de _PAGES_ (_TOTAL_ registros)",
        "infoEmpty": "No hay registros disponibles.",
        "search": "Buscar",
        "thousands": ",",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "emptyTable": "No hay registros disponibles",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "infoFiltered": "(filtrados de _MAX_ registros totales)"
    },

    validarCampo: function (Control) {
        /// <summary>
        /// Valida un campo obligatorio
        /// </summary>
        /// <param name="#IdControl" type="String">Id del control a aplicar</param>
        tipo = $(Control);
        if (tipo[0].localName == "input") {
            if ($(Control).next().hasClass("input-group-append") || $(Control).prev().hasClass("input-group-prepend") || $(Control).next().hasClass("input-group")) {
                if ($.trim($(Control).val()) == "") {
                    $(Control).parent().parent().addClass("has-error");
                    $(Control).parent().next().removeClass("hide");
                    return false;
                } else {
                    $(Control).parent().parent().removeClass("has-error");
                    $(Control).parent().next().addClass("hide");
                    return true;
                }
            } else {
                if ($.trim($(Control).val()) == "") {
                    $(Control).parent().addClass("has-error");
                    $(Control).next().removeClass("hide");
                    return false;
                } else {
                    $(Control).parent().removeClass("has-error");
                    $(Control).next().addClass("hide");
                    return true;
                }
            }
        }
        if (tipo[0].localName == "textarea") {
            if ($.trim($(Control).val()) == "") {
                $(Control).parent().addClass("has-error");
                $(Control).next().removeClass("hide");
                return false;
            } else {
                $(Control).parent().removeClass("has-error");
                $(Control).next().addClass("hide");
                return true;
            }
        }
        if (tipo[0].localName == "select") {
            if ($(Control).hasClass("select")) {
                if ($(Control).attr("multiple") == "multiple") {
                    if ($(Control).val() == null) {
                        $(Control).parent().addClass("has-error");
                        $(Control).next().next().removeClass("hide");
                        return false;
                    } else {
                        $(Control).parent().removeClass("has-error");
                        $(Control).next().next().addClass("hide");
                        return true;
                    }
                } else {
                    if ($(Control).val() == 0) {
                        $(Control).parent().addClass("has-error");
                        $(Control).next().removeClass("hide");
                        $(Control).next().children().children().addClass("select-error");
                        return false;
                    } else {
                        $(Control).parent().removeClass("has-error");
                        $(Control).next().addClass("hide");
                        $(Control).next().children().children().removeClass("select-error");
                        return true;
                    }
                }

            } else {
                if ($(Control).val() == 0) {
                    $(Control).parent().addClass("has-error");
                    $(Control).next().removeClass("hide");
                    return false;
                } else {
                    $(Control).parent().removeClass("has-error");
                    $(Control).next().addClass("hide");
                    return true;
                }
            }

        }

    },
    limpiarCampo: function (Control) {
        /// <summary>
        /// Limpia un campo
        /// </summary>
        /// <param name="#IdControl" type="String">Id del control a aplicar</param>
        tipo = $(Control);
        if (tipo[0].localName == "input") {
            if ($(Control).next().hasClass("input-group-append") || $(Control).prev().hasClass("input-group-prepend") || $(Control).next().hasClass("input-group")) {
                $(Control).val("");
                $(Control).parent().parent().removeClass("has-error");
                $(Control).parent().next().addClass("hide");
            } else {
                $(Control).val("");
                $(Control).parent().removeClass("has-error");
                $(Control).next().addClass("hide");
            }
        }
        if (tipo[0].localName == "textarea") {
            $(Control).val("");
            $(Control).parent().removeClass("has-error");
            $(Control).next().addClass("hide");
        }
        if (tipo[0].localName == "select") {
            if ($(Control).hasClass("select2")) {
                if ($(Control).attr("multiple") == "multiple") {
                    $(Control + " > option").removeAttr("selected");
                    $(Control).trigger("change");
                    $(Control).parent().removeClass("has-error");
                    $(Control).next().next().addClass("hide");
                } else {
                    $(Control).val(0).trigger("change");
                    $(Control).parent().removeClass("has-error");
                    $(Control).next().next().addClass("hide");
                    $(Control).next().children().children().removeClass("select-error");
                }

            } else {
                $(Control).val(0);
                $(Control).parent().removeClass("has-error");
                $(Control).next().addClass("hide");
            }

        }

    },
    ToastError: function (Mensaje) {
        setTimeout(function () {
            $.toast({
                text: Mensaje,
                position: 'top-right',
                loaderBg: '#bf441d',
                icon: 'error',
                hideAfter: 4000,
                stack: 1
            });
        }, 250);
    },
    ToastSuccess: function (Mensaje) {
        setTimeout(function () {
            $.toast({
                text: Mensaje,
                position: 'top-right',
                loaderBg: '#5ba035',
                icon: 'success',
                hideAfter: 3000,
                stack: 1
            });
        }, 250);
    },
    ToastWarning: function (Mensaje) {
        setTimeout(function () {
            $.toast({
                text: Mensaje,
                position: 'top-right',
                loaderBg: '#da8609',
                icon: 'warning',
                hideAfter: 3000,
                stack: 1
            });
        }, 250);
    },
    ToastWarningTimeOut: function (Mensaje, TimeOut) {
        setTimeout(function () {
            $.toast({
                text: Mensaje,
                position: 'top-right',
                loaderBg: '#da8609',
                icon: 'warning',
                hideAfter: TimeOut,
                stack: 1
            });
        }, 250);
    },
    ToastInfo: function (Mensaje) {
        setTimeout(function () {
            $.toast({
                text: Mensaje,
                position: 'top-right',
                loaderBg: '#3b98b5',
                icon: 'info',
                hideAfter: 3000,
                stack: 1
            });
        }, 250);
    },
}

var BLOCKUI = {
    //Funciones util.factory
    blockUIStar: function () {
        
        $(".gifLoadingGeneral").show();
    },
    blockUIStop: function () {
        //$.unblockUI();
        $(".gifLoadingGeneral").hide();
    },
}