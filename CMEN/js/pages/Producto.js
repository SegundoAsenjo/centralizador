﻿

var Eventos = {
    ListComboInsumo: function () {
        $.ajax({
            url: urlListInsumo,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                Funciones.LlenarComboInsumo(response, "#cboInsumo");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
    },
    ListComboDespacho: function () {
        $.ajax({
            url: urlListDespacho,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                Funciones.LlenarComboDespacho(response, "#cboDespacho");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
    },
    ListComboLinea: function () {
        $.ajax({
            url: urlListLinea,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                Funciones.LlenarComboLinea(response, "#cboLinea");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
    },
    ListComboCategoria: function () {
        var IdLinea = $("#cboLinea").val();
        $.ajax({
            url: urlListCategoria,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ "IdLinea": IdLinea }),
            success: function (response) {
                Funciones.LlenarComboCategoria(response, "#cboCategoria");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
    },
    ListComboUnidad: function () {
        $.ajax({
            url: urlListUnidadMedida,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                Funciones.LlenarComboUnidad(response, "#cboUnidadMedida");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
    },

    SaveProducto: function () {

        var request = new Object();

        request.Estado_Producto = $("#cboEstado option:selected").text();
        request.Nombre_Producto = $("#txtProducto").val().trim().toUpperCase();
        request.Linea_id = $("#cboLinea").val();
        request.Descripcion = $("#txtDescripcion").val().trim().toUpperCase();
        request.Categoria_id = $("#cboCategoria").val();
        request.Categoria_id_ref = $("#cboCategoria option:selected").attr("data-IdCategoriaRef");
        request.UnidadMedida_id_ref = $("#cboUnidadMedida option:selected").attr("data-IdUnidadRef");
        request.UnidadMedida_id = $("#cboUnidadMedida").val();
        request.Fecha_ingreso = $("#txtFechaIngreso").val();
        request.Stock = $("#txtStock").val();
        request.PrecioCosto = $("#txtPrecioCosto").val();
        request.PrecioVenta = $("#txtPrecioVenta").val();
        request.Producto_id = $("#txtIdProducto").val();
        request.Impresora_id = $("#cboDespacho").val();
        request.Producto_id_ref = $("#txtIdProductoIdRef").val();
        if (request.Producto_id == "0") {
            request.EstadoAct = 0;
        } else {
            request.EstadoAct = 2;
        }


        $.ajax({
            url: urlSaveProducto,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ "request": request }),
            success: function (response) {

                if (response.IsSuccess) {
                    $('#grillaProductos').DataTable().ajax.reload();
                    $("#ModalAgregarProducto").modal("hide");
                    CMEN.ToastSuccess("Se guardo el producto correctamente.");
                } else {
                    CMEN.ToastError("El producto no se guarda");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    },
    GetProducto: function (IdProducto, NombreProducto, Descripcion, IdLinea, IdCategoria, IdUnidadMedida, FechaIngreso, Stock, PrecioCosto, PrecioVenta, Estado, IdImpresora, IdProductoRef) {

        Funciones.LimpiarAgregarProducto();
        var IdLinea = IdLinea;
        if (Estado == "ACTIVO") {
            $("#cboEstado").val(1);
        }
        else {
            $("#cboEstado").val(2);
        }
        $("#txtIdProductoIdRef").val(IdProductoRef);
        $("#txtIdProducto").val(IdProducto);
        $("#cboEstado").prop("disabled", false);
        $("#cboLinea").val(IdLinea).trigger("change");
        $("#txtProducto").val(NombreProducto);
        $("#txtDescripcion").val(Descripcion);
        $("#cboUnidadMedida").val(IdUnidadMedida).trigger("change");
        $("#txtFechaIngreso").val(FechaIngreso);
        $("#txtStock").val(Stock);
        $("#txtPrecioCosto").val(PrecioCosto);
        $("#txtPrecioVenta").val(PrecioVenta);
        $("#cboDespacho").val(IdImpresora);

        $.ajax({
            url: urlListCategoria,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ "IdLinea": IdLinea }),
            success: function (response) {
                Funciones.LlenarComboCategoria(response, "#cboCategoria");
                $("#cboCategoria").val(IdCategoria).trigger("change");
                $("#titleProducto").html("Editar Producto");
                $("#ModalAgregarProducto").modal("show");

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
    },
    Agregarinsumos: function (IdProducto, nombreProducto) {
        $("#titleDetalleInsumo").html(nombreProducto);
        $("#modalAgregarInsumos").modal("show");
    },


}

var Funciones = {

    RecargarDatosPorCambioEmpresa: function () {
        $("#grillaProductos").DataTable().ajax.reload();
        Eventos.ListComboLinea();
        Eventos.ListComboUnidad();
        if ($("#cboLocal").val() != "0") {
            $("#botones").removeClass("hide");
        }
        else {
            $("#botones").addClass("hide");
        }
    },

    LlenarComboLinea: function (data, control) {
        $(control).html("");
        var Opciones = '<option value="0">Seleccione linea</option>';
        $.each(data, function (e, i) {
            Opciones = Opciones + '<option value="' + i.Linea_id + '">' + i.Nombre + '</option>';
        });
        $(control).html(Opciones);
    },
    LlenarComboDespacho: function (data, control) {
        $(control).html("");
        var Opciones = '<option value="0">Seleccione despacho</option>';
        $.each(data, function (e, i) {
            Opciones = Opciones + '<option value="' + i.Impresora_id + '">' + i.Nombre + '</option>';
        });
        $(control).html(Opciones);
    },

    LlenarComboCategoria: function (data, control) {
        $(control).html("");
        var Opciones = '<option value="0">Seleccione categoria</option>';
        $.each(data, function (e, i) {
            Opciones = Opciones + '<option value="' + i.Categoria_id + '" ' + ' data-IdCategoriaRef="' + i.Categoria_id_ref + '">' + i.Nombrecategoria + '</option>';
        });
        $(control).html(Opciones);
    },
    LlenarComboUnidad: function (data, control) {
        $(control).html("");
        var Opciones = '<option value="0">Seleccione Unidad</option>';
        $.each(data, function (e, i) {
            Opciones = Opciones + '<option value="' + i.Unidad_medida_id + '" ' + ' data-IdUnidadRef="' + i.Unidad_medidad_id_ref + '">' + i.Nombre_medida + '</option>';
        });
        $(control).html(Opciones);
    },
    LlenarComboInsumo: function (data, control) {
        $(control).html("");
        var Opciones = '<option value="0">Seleccione insumo</option>';
        $.each(data, function (e, i) {
            Opciones = Opciones + '<option data-unidadMedida="' + i.NombreUnidadMedida + '" value="' + i.Insumo_id + '">' + i.Nombre_insumo + '</option>';
        });
        $(control).html(Opciones);
    },

    LimpiarAgregarProducto: function () {
        $("#txtIdProducto").val("0");
        $("#txtIdProductoIdRef").val("0");
        CMEN.limpiarCampo("#txtProducto");
        CMEN.limpiarCampo("#txtDescripcion");
        CMEN.limpiarCampo("#cboCategoria");
        CMEN.limpiarCampo("#cboUnidadMedida");
        CMEN.limpiarCampo("#cboDespacho");
        CMEN.limpiarCampo("#cboLinea");
        CMEN.limpiarCampo("#txtFechaIngreso");
        CMEN.limpiarCampo("#txtStock");
        CMEN.limpiarCampo("#txtPrecioCosto");
        CMEN.limpiarCampo("#txtPrecioVenta");
        $("#cboCategoria").html("<option value='0'>Seleccione categoria</option>");

    },

}

var Grillas = {
    ListProductoPaginado: function () {
        $('#grillaProductos').DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": false,
            "autoWidth": true,
            "destroy": true,
            "scrollY": "calc(100vh - 350px)",
            "scrollX": true,
            "language": CMEN.languageDatatable,
            "ajax": {
                "url": urlListProductoPaginado,
                "type": 'POST',
                "datatype": "json",
                "contentType": 'application/json; charset=UTF-8',
                "dataSrc": "",
                "data": function (sSource) {
                    return JSON.stringify({ "page": sSource });
                },
            },
            "columns": [
                { "data": "Nombre_Producto", "sClass": 'text-center' },
                { "data": "NombreCategoria", "sClass": 'text-center' },
                { "data": "Stock", "sClass": 'text-center' },
                { "data": "NombreUnidadMedida", "sClass": 'text-center' },
                { "data": "PrecioCosto", "sClass": 'text-center' },
                { "data": "PrecioVenta", "sClass": 'text-center' },
                {
                    "data": function (row, type, set, meta) {
                        let Estado = '';
                        switch (row.EstadoAct) {
                            case 0:
                                Estado = '<div class="fondoPendiente">Pendiente</div>';
                                break;
                            case 1:
                                Estado = '<div class="fondoVigente">Actualizado</div>';
                                break;
                            case 2:
                                Estado = '<div class="fondoPendiente">Pendiente</div>';
                                break;
                        }
                        return Estado
                    }, "bSortable": false, "sClass": 'text-center'
                },
                {
                    "data": function (row, type, set, meta) {
                        var editar = '<a data-toggle="tooltip" data-placement="bottom" title="Editar" onclick="Eventos.GetProducto(\'' + row.Producto_id + '\',\'' + row.Nombre_Producto + '\',\'' + row.Descripcion + '\',\'' + row.Linea_id + '\',\'' + row.Categoria_id + '\',\'' + row.UnidadMedida_id + '\',\'' + row.Fecha_ingreso + '\',' + row.Stock + ',' + row.PrecioCosto + ',' + row.PrecioVenta + ',\'' + row.Estado_Producto + '\',\'' + row.Impresora_id + '\',\'' + row.Producto_id_ref + '\')" ><i class="fa fa-pencil-alt"></i></a>';
                        var insumos = '<a data-toggle="tooltip" data-placement="bottom" title="Agregar insumos" onclick="Eventos.Agregarinsumos(\'' + row.Producto_id + '\',\'' + row.Nombre_Producto + '\')" ><i class="fa fa-clipboard-list"></i></a>';
                        if (row.EstadoAct != 1) {
                            editar = '';
                            insumos = '';
                        }
                        return editar + ' ' + insumos;
                    }, "bSortable": false, "sClass": 'text-center'
                }
            ],
            "lengthMenu": [50, 100, 200],
            "pageLength": 50,
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip({ container: 'body' });
            }
        });
    },
    ListInsumoPaginado: function () {
        $('#grillaInsumos').DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": false,
            "autoWidth": false,
            "destroy": true,
            "language": CMEN.languageDatatable,
            "ajax": {
                "url": urlListInsumoPaginado,
                "type": 'POST',
                "datatype": "json",
                "contentType": 'application/json; charset=UTF-8',
                "dataSrc": "",
                "data": function (sSource) {
                    var IdAperturaCierre = $("#txtIdAperturaCierreCaja").val();
                    return JSON.stringify({ "page": sSource, "IdAperturaCierre": IdAperturaCierre });
                },
            },
            "columns": [

                { "data": "Tipo_documento", "sClass": 'text-center' },
                { "data": "Razon_social", "sClass": 'text-center' },
                {
                    "data": function (row, type, set, meta) {
                        var detalle = '<a data-toggle="tooltip" data-placement="bottom" title="Detalle" onclick="Eventos.DetalleVentas(\'' + row.Pedido_ID + '\')" ><i class="fa fa-file-alt"></i></a>';
                        return detalle;
                    }, "bSortable": false, "sClass": 'text-center'
                }
            ],
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip({ container: 'body' });
            }
        });
    },
}

$(document).ready(function () {

    //$('#btnCrearProducto').popover({
    //    placement: "bottom",
    //    container: 'body',
    //    trigger: "hover",
    //    content: "Asignar insumos a este producto."
    //});
    
    $("#txtProducto").keyup(function (e) {
        $("#txtDescripcion").val($("#txtProducto").val());
    });
    $("#cboInsumo").on("change", function () {
        $("#txtUnidadMedida").val($("#cboInsumo option:selected").attr("data-unidadMedida"));
    });

    $("#txtPrecioCosto").numericInput({ allowFloat: true, allowNegative: false, max: 200, min: 0 });
    $("#txtPrecioVenta").numericInput({ allowFloat: true, allowNegative: false, max: 200, min: 0 });
    $("#txtCantidad").numericInput({ allowFloat: true, allowNegative: false, max: 200, min: 0 });

    Grillas.ListProductoPaginado();
    Eventos.ListComboLinea();
    Eventos.ListComboUnidad();
    Eventos.ListComboDespacho();
    Eventos.ListComboInsumo();

    if ($("#cboLocal").val() != "0") {
        $("#botones").removeClass("hide");
    }
    else {
        $("#botones").addClass("hide");
    }

    $("#btnAgregarProducto").on("click", function () {
        $("#titleProducto").html("Agregar Producto");
        Funciones.LimpiarAgregarProducto();
        $("#cboEstado").val(1);
        $("#cboEstado").prop("disabled", true);
        $("#ModalAgregarProducto").modal("show");
    });

    $("#btnRecargarProducto").on("click", function () {
        $('#grillaProductos').DataTable().ajax.reload();
    });

    $("#cboLinea").on("change", function () {
        Eventos.ListComboCategoria();
    });

    $("#btnGuardarProducto").on("click", function () {
        var validar = true;
        if (!CMEN.validarCampo("#txtProducto")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#cboCategoria")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#cboUnidadMedida")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#cboLinea")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#txtFechaIngreso")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#txtStock")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#txtPrecioCosto")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#txtPrecioVenta")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#cboEstado")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#cboDespacho")) {
            validar = false;
        }
        if (validar) {
            Eventos.SaveProducto();
        }
    });

    $("#btnAgregarInsumos").on("click", function () {
        if (!CMEN.validarCampo("#cboInsumo")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#txtCantidad")) {
            validar = false;
        }
        if (validar) {
            Eventos.SaveInsumo();
        }
    });



});