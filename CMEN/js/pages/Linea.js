﻿
var Eventos = {

    SaveLinea: function () {
        var request = new Object();

        request.Nombre = $("#txtLinea").val().trim().toUpperCase();
        request.Linea_id = $("#txtIdLinea").val();
        request.Estado = $("#cboEstado option:selected").text();
        request.Linea_id_ref = $("#txtIdLineaRef").val();
        if (request.Linea_id == "0") {
            request.EstadoAct = 0;
        } else {
            request.EstadoAct = 2;
        }

        $.ajax({
            url: urlSaveLinea,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ "request": request }),
            success: function (response) {

                if (response.IsSuccess) {
                    $('#grillaLinea').DataTable().ajax.reload();
                    $("#ModalAgregarLinea").modal("hide");
                } else {

                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    },
    GetLinea: function (IdLinea, NombreLinea, estado, IdLineaRef) {

        Funciones.LimpiarAgregarLinea();
        $("#txtIdLinea").val(IdLinea);
        $("#txtIdLineaRef").val(IdLineaRef);
        $("#txtLinea").val(NombreLinea);
        if (estado == "ACTIVO") {
            $("#cboEstado").val(1);
        }
        else {
            $("#cboEstado").val(2);
        }
        $("#cboEstado").prop("disabled", false);
        $("#titleLinea").html("Editar Linea");
        $("#ModalAgregarLinea").modal("show");
        
    },

    
}

var Funciones = {
    RecargarDatosPorCambioEmpresa: function () {
        $("#grillaLinea").DataTable().ajax.reload();

        if ($("#cboLocal").val() != "0") {
            $("#botones").removeClass("hide");
        }
        else {
            $("#botones").addClass("hide");
        }
    },
    LimpiarAgregarLinea: function () {
        $("#txtIdLinea").val("0");
        $("#txtIdLineaRef").val("0");
        CMEN.limpiarCampo("#txtLinea");
    },
}

var Grillas = {
    ListLineaPaginado: function () {
        $('#grillaLinea').DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": false,
            "autoWidth": true,
            "destroy": true,
            "scrollY": "calc(100vh - 350px)",
            "scrollX": true,
            "language": CMEN.languageDatatable,
            "ajax": {
                "url": urlListLineaPaginado,
                "type": 'POST',
                "datatype": "json",
                "contentType": 'application/json; charset=UTF-8',
                "dataSrc": "",
                "data": function (sSource) {
                    return JSON.stringify({ "page": sSource });
                },
            },
            "columns": [
                { "data": "Nombre", "sClass": 'text-left' },
                { "data": "Estado", "sClass": 'text-center' },
                {
                    "data": function (row, type, set, meta) {
                        let Estado = '';
                        switch (row.EstadoAct) {
                            case 0:
                                Estado = '<div class="fondoPendiente">Pendiente</div>';
                                break;
                            case 1:
                                Estado = '<div class="fondoVigente">Actualizado</div>';
                                break;
                            case 2:
                                Estado = '<div class="fondoPendiente">Pendiente</div>';
                                break;
                        }
                        return Estado
                    }, "bSortable": false, "sClass": 'text-center'
                },
                {
                    "data": function (row, type, set, meta) {
                        var editar = '<a data-toggle="tooltip" data-placement="bottom" title="Editar" onclick="Eventos.GetLinea(\'' + row.Linea_id + '\',\'' + row.Nombre + '\',\'' + row.Estado + '\',\'' + row.Linea_id_ref +'\')" ><i class="fa fa-pencil-alt"></i></a>';
                        if (row.EstadoAct != 1) {
                            editar = '';
                        }
                        return editar;
                    }, "bSortable": false, "sClass": 'text-center'
                }
            ],
            "lengthMenu": [50, 100, 200],
            "pageLength": 50,
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip({ container: 'body' });
            }
        });
    },
}

$(document).ready(function () {

    Grillas.ListLineaPaginado();

    if ($("#cboLocal").val() != "0") {
        $("#botones").removeClass("hide");
    }
    else {
        $("#botones").addClass("hide");
    }
    $("#btnRecargarLinea").on("click", function () {
        $('#grillaLinea').DataTable().ajax.reload();
    });

    $("#btnAgregarLinea").on("click", function () {
        $("#titleLinea").html("Agregar Linea");
        Funciones.LimpiarAgregarLinea();
        $("#cboEstado").val(1);
        $("#cboEstado").prop("disabled", true);
        $("#ModalAgregarLinea").modal("show");
    });

    $("#btnGuardarLinea").on("click", function () {
        var validar = true;
        if (!CMEN.validarCampo("#txtLinea")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#cboEstado")) {
            validar = false;
        }
        if (validar) {
            Eventos.SaveLinea();
        }
    });
});