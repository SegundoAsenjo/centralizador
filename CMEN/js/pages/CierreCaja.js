﻿

var Eventos = {


    VentasXCierreCaja: function (IdAperturaCierreCaja) {
        $("#txtIdAperturaCierreCaja").val(IdAperturaCierreCaja);
        $("#grillaVentasXCierreCaja").DataTable().ajax.reload();

        $("#modalVentasXCierreCaja").modal("show");

    },

    DetalleVentas: function (IdVenta) {
        $("#txtIdVenta").val(IdVenta);
        $("#grillaDetalleVenta").DataTable().ajax.reload();

        $("#modalDetalleVenta").modal("show");

    },

}

var Funciones = {

    RecargarDatosPorCambioEmpresa: function () {
        let validar = true;
        if ($("#txtFechaInicio").val == "") {
            validar = false;
        }
        if ($("#txtFechaFin").val() == "") {
            validar = false;
        }
        if (validar) {
            $("#grillaCierreCaja").DataTable().ajax.reload();
        }
        
        if ($("#cboLocal").val() != "0") {
            $("#botones").removeClass("hide");
        }
        else {
            $("#botones").addClass("hide");
        }
    },

}

var Grillas = {
    ListCierreCajaPaginado: function () {
        $('#grillaCierreCaja').DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": false,
            "autoWidth": true,
            "destroy": true,
            "scrollY": "calc(100vh - 350px)",
            "scrollX": true,
            "language": CMEN.languageDatatable,
            "ajax": {
                "url": urlListCierreCajaXLocalPaginado,
                "type": 'POST',
                "datatype": "json",
                "contentType": 'application/json; charset=UTF-8',
                "dataSrc": "",
                "data": function (sSource) {
                    var request = new Object();
                    request.FechaInicio = $("#txtFechaInicio").val();
                    request.FechaFin = $("#txtFechaFin").val();
                    return JSON.stringify({ "page": sSource, "request": request});
                },
            },
            "columns": [
                { "data": "FechaCaja", "sClass": 'text-center' },
                { "data": "Horaini", "sClass": 'text-center' },
                { "data": "Montoinicial", "sClass": 'text-center' },
                { "data": "Totalefect", "sClass": 'text-center' },
                { "data": "Totalcompras", "sClass": 'text-center' },
                { "data": "Totalpagos", "sClass": 'text-center' },
                {
                    "data": function (row, type, set, meta) {
                        var detalle = '<a data-toggle="tooltip" data-placement="bottom" title="Detalle" onclick="Eventos.VentasXCierreCaja(\'' + row.IdAperturacierre +'\')" ><i class="fa fa-file-alt"></i></a>';
                        return detalle;
                    }, "bSortable": false, "sClass": 'text-center'
                }
            ],
            "lengthMenu": [50, 100, 200],
            "pageLength": 50,
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip({ container: 'body' });
            }
        });
    },

    ListVentaXCierreCajaPaginado: function () {
        $('#grillaVentasXCierreCaja').DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": false,
            "autoWidth": false,
            "destroy": true,
            "language": CMEN.languageDatatable,
            "ajax": {
                "url": urlListVentaXCierreCajaPaginado,
                "type": 'POST',
                "datatype": "json",
                "contentType": 'application/json; charset=UTF-8',
                "dataSrc": "",
                "data": function (sSource) {
                    var IdAperturaCierre = $("#txtIdAperturaCierreCaja").val();
                    return JSON.stringify({ "page": sSource, "IdAperturaCierre": IdAperturaCierre });
                },
            },
            "columns": [

                { "data": "Tipo_documento", "sClass": 'text-center'},
                { "data": "NroSerie", "sClass": 'text-center'},
                { "data": "Cliente", "sClass": 'text-center' },
                { "data": "Personal", "sClass": 'text-center' },
                { "data": "Total", "sClass": 'text-center' },
                { "data": "Razon_social", "sClass": 'text-center' },
                {
                    "data": function (row, type, set, meta) {
                        var detalle = '<a data-toggle="tooltip" data-placement="bottom" title="Detalle" onclick="Eventos.DetalleVentas(\'' + row.Pedido_ID + '\')" ><i class="fa fa-file-alt"></i></a>';
                        return detalle;
                    }, "bSortable": false, "sClass": 'text-center'
                }
            ],
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip({ container: 'body' });
            }
        });
    },

    ListDetalleVentaPaginado: function () {
        $('#grillaDetalleVenta').DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": false,
            "autoWidth": false,
            "destroy": true,
            "language": CMEN.languageDatatable,
            "ajax": {
                "url": urlListDetalleVentaPaginado,
                "type": 'POST',
                "datatype": "json",
                "contentType": 'application/json; charset=UTF-8',
                "dataSrc": "",
                "data": function (sSource) {
                    var IdVenta = $("#txtIdVenta").val();
                    return JSON.stringify({ "page": sSource, "IdVenta": IdVenta });
                },
            },
            "columns": [

                { "data": "DescripcionProd", "sClass": 'text-center' },
                { "data": "Cantidad", "sClass": 'text-center' },
                { "data": "PrecioUnitario", "sClass": 'text-center' },
                { "data": "Igvd", "sClass": 'text-center' },
                { "data": "Subtotald", "sClass": 'text-center' },
            ],
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip({ container: 'body' });
            }
        });
    },

    
}

$(document).ready(function () {

    if ($("#cboLocal").val() != "0") {
        $("#botones").removeClass("hide");

        let validar = true;
    }
    else {
        $("#botones").addClass("hide");
    }
    $("#btnFiltrarXFecha").on("click", function () {
        var validar = true;
        if (!CMEN.validarCampo("#txtFechaInicio")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#txtFechaFin")) {
            validar = false;
        }
        if ($("#txtFechaInicio").val() != "" && $("#txtFechaFin").val() != "")
        {
            var strFechaInicio = $("#txtFechaInicio").val().split('-');
            var strFechaFin = $("#txtFechaFin").val().split('-');
            var FechaInicio = new Date(strFechaInicio[2], parseInt(strFechaInicio[1]) - 1, strFechaInicio[0]);
            var FechaFin = new Date(strFechaFin[2], parseInt(strFechaFin[1]) - 1, strFechaFin[0]);
            if (FechaInicio > FechaFin) {
                //GISSAT.ToastError(Message.fechaFinMenor);
                $("#txtFechaFin").parent().parent().addClass("has-error");
                Validar = false;
            } else {
                $("#txtFechaFin").parent().parent().removeClass("has-error");
            }
        }
        if (validar) {
            Grillas.ListCierreCajaPaginado();
        }
    });
    
    Grillas.ListVentaXCierreCajaPaginado();
    Grillas.ListDetalleVentaPaginado();

    $("#modalDetalleVenta").on("hidden.bs.modal", function () {
        $("body").addClass("modal-open");
    });

});