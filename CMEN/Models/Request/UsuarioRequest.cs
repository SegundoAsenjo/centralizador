﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMEN.Models.Request
{
    public class UsuarioRequest
    {
        public string NombreUsuario { get; set; }
        public string Contraseña { get; set; }
        public string IdNegocio { get; set; }
    }
}