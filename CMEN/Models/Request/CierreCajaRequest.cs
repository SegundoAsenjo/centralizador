﻿using System;

namespace CMEN.Models
{
    public class CierreCajaRequest
    {
        public int IdCierreCaja { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
    }
}