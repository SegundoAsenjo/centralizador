﻿using CMEN.Models;
using CMEN.Models.Request;
using CMEN.sr_centralizador;
using System.Linq;
using System.Web.Mvc;

namespace CMEN.Controllers
{
    public class AccountController : Controller
    {

        ws_cmenSoapClient servicio = new ws_cmenSoapClient();
        Credenciales credenciales = new Credenciales();

        public AccountController()
        {
            credenciales.username = Configuracion.UsuarioServicio;
            credenciales.password = Configuracion.PasswordServicio;
        }

        // GET: Login
        public ActionResult Login()
        {
            Session["RutaLogoNegocio"] = "";
            return View();
        }

        public ActionResult ListNegocio()
        {
            

            var xml = servicio.getNegocio(credenciales);
            foreach(var item in xml)
            {
                item.Img = string.Format("data:image/png;base64,{0}", item.Img);
            }

            return Json(xml, JsonRequestBehavior.AllowGet);
        }

        public ActionResult IngresarMenu(UsuarioRequest request)
        {
            Result result = new Result();
            Usuario user = new Usuario();
            user.Idnegocio = request.IdNegocio;
            user.UsuarioN = request.NombreUsuario;
            user.Contra = request.Contraseña;

            user = servicio.verificarUsuxNeg(credenciales, user.UsuarioN, user.Contra, user.Idnegocio);

            if (user.Estado == "ACTIVO")
            {
                var xml = servicio.getLocalesxUsu(user).ToList();

                Session["IdUsuario"] = user.Idusuario;
                Session["IdNegocio"] = user.Idnegocio;
                Session["NombreUsuario"] = user.UsuarioN;
                Session["IdLocal"] = "0";
                Session["ComboLocal"] = xml;

                result.IsSuccess = true;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                result.IsSuccess = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

 
        }
        public ActionResult LogOut()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Login", "Account");
        }


    }
}