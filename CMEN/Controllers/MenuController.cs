﻿using CMEN.Models;
using CMEN.sr_centralizador;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CMEN.Controllers
{
    public class MenuController : Controller
    {

        ws_cmenSoapClient servicio = new ws_cmenSoapClient();
        Credenciales credenciales = new Credenciales();
        
        public MenuController()
        {
            credenciales.username = Configuracion.UsuarioServicio;
            credenciales.password = Configuracion.PasswordServicio;

        }

        #region Vistas
        public ActionResult Inicio()
        {
            return View();
        }

        public ActionResult CierreCaja()
        {
            return View();
        }

        public ActionResult Productos()
        {
            return View();
        }

        public ActionResult Categoria()
        {
            return View();
        }
        public ActionResult Linea()
        {
            return View();
        }
        public ActionResult UnidadMedida()
        {
            return View();
        }
        #endregion

        #region CierreCaja
        [HttpPost]
        public ActionResult ListCierreCajaXLocalPaginado(CierreCajaRequest request)
        {
            Local local = new Local();
            List<AperturaCierreCaja> cierreCaja = new List<AperturaCierreCaja>();

            local.Idlocal = (string)Session["IdLocal"];
            if (local.Idlocal != "0")
            {
                cierreCaja = servicio.getAperturaxFecha(credenciales, local.Idlocal, request.FechaInicio, request.FechaFin).ToList();
            }

            return Json(cierreCaja, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult ListVentaXCierreCajaPaginado(string IdAperturaCierre)
        {
            AperturaCierreCaja cierreCaja = new AperturaCierreCaja();
            List<Orden_Pedido> ventas = new List<Orden_Pedido>();

           if(IdAperturaCierre != "0")
            {
                cierreCaja.IdAperturacierre = IdAperturaCierre;
                ventas = servicio.getOrden_Pedidos(cierreCaja).ToList();
            }
            
            return Json(ventas, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListDetalleVentaPaginado(string IdVenta)
        {
            Orden_Pedido venta = new Orden_Pedido();
            List<Detalle_Orden_Pedido> detalleVenta = new List<Detalle_Orden_Pedido>();
            if(IdVenta != "0")
            {
                venta.Pedido_ID = IdVenta;
                detalleVenta = servicio.getDetalle_Orden_Pedidos(venta).ToList();
            }
            
            return Json(detalleVenta, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region General

        [HttpPost]
        public ActionResult CambiarLocal(string IdLocal)
        {
            Session["IdLocal"] = IdLocal;

            Result result = new Result();
            result.IsSuccess = true;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListComboLinea()
        {
            Local local = new Local();
            List<Linea> linea = new List<Linea>();

            local.Idlocal = (string)Session["IdLocal"];
            if (local.Idlocal != "0")
            {
                linea = servicio.GetLineas(credenciales, local.Idlocal).Where(x => x.Estado == "ACTIVO").ToList();
            }

            return Json(linea, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListComboCategoria(string IdLinea)
        {
            Linea linea = new Linea();
            List<Categoria> categoria = new List<Categoria>();

            linea.Linea_id = IdLinea;
            if (linea.Linea_id != "0")
            {
                categoria = servicio.GetCategorias(credenciales, linea.Linea_id).Where(x => x.Estado == "ACTIVO").ToList();
            }

            return Json(categoria, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListComboUnidadMedida()
        {
            Local local = new Local();
            List<Unidad_Medida> unidadMedida = new List<Unidad_Medida>();

            local.Idlocal = (string)Session["IdLocal"];
            if (local.Idlocal != "0")
            {
                unidadMedida = servicio.GetUnidad_Medidas(credenciales, local.Idlocal).Where(x => x.Estado == "ACTIVO").ToList();
            }

            return Json(unidadMedida, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ListComboDespacho()
        {
            Local local = new Local();
            List<Impresora> despacho = new List<Impresora>();

            local.Idlocal = (string)Session["IdLocal"];
            if (local.Idlocal != "0")
            {
                despacho = servicio.GetImpresoras(local.Idlocal).ToList();
            }

            return Json(despacho, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ListComboInsumo()
        {
            Local local = new Local();
            List<Insumo> insumo = new List<Insumo>();

            local.Idlocal = (string)Session["IdLocal"];
            if (local.Idlocal != "0")
            {
                insumo = servicio.GetInsumos(local.Idlocal).Where(x => x.Estado_insumo == "ACTIVO").ToList();
            }

            return Json(insumo, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Productos

        [HttpPost]
        public ActionResult ListProductoPaginado()
        {
            Local local = new Local();
            List<Producto> producto = new List<Producto>();

            local.Idlocal = (string)Session["IdLocal"];
            if (local.Idlocal != "0")
            {
                producto = servicio.GetProductos(credenciales, local.Idlocal).ToList();
            }

            return Json(producto, JsonRequestBehavior.AllowGet);

        }

        
        [HttpPost]
        public ActionResult ListCategoria(string IdLinea)
        {
            Linea linea = new Linea();
            List<Categoria> categoria = new List<Categoria>();

            linea.Linea_id = IdLinea;
            if (linea.Linea_id != "0")
            {
                categoria = servicio.GetCategorias(credenciales, linea.Linea_id).ToList();
            }

            return Json(categoria, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveProducto(Producto request)
        {

            request.Moneda = "SOLES";
            request.Idlocal = (string)Session["IdLocal"];
            var response = request.Producto_id == "0" ? servicio.SetProducto(request) : servicio.UpdateProducto(request);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Linea

        [HttpPost]
        public ActionResult ListLinea()
        {
            Local local = new Local();
            List<Linea> linea = new List<Linea>();

            local.Idlocal = (string)Session["IdLocal"];
            if (local.Idlocal != "0")
            {
                linea = servicio.GetLineas(credenciales, local.Idlocal).ToList();
            }

            return Json(linea, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveLinea(Linea request)
        {

            request.Idlocal = (string)Session["IdLocal"];
            var respuesta = request.Linea_id == "0" ?
                servicio.SetLinea(request) :
                servicio.UpdateLinea(request);


            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Categoria
        [HttpPost]
        public ActionResult SaveCategoria(Categoria request)
        {

            var respuesta = request.Categoria_id == "0" ?
                servicio.SetCategoria(request) :
                servicio.UpdateCategoria(request);


            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Unidad Medida
        [HttpPost]
        public ActionResult ListUnidadMedida()
        {
            Local local = new Local();
            List<Unidad_Medida> unidadMedida = new List<Unidad_Medida>();

            local.Idlocal = (string)Session["IdLocal"];
            if (local.Idlocal != "0")
            {
                unidadMedida = servicio.GetUnidad_Medidas(credenciales, local.Idlocal).ToList();
            }

            return Json(unidadMedida, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveUnidadMedida(Unidad_Medida request)
        {

            request.Idlocal = (string)Session["IdLocal"];
            var respuesta = request.Unidad_medida_id == "0" ?
                servicio.SetUnidadMedida(request) :
                servicio.UpdateUnidadMedida(request);

            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}