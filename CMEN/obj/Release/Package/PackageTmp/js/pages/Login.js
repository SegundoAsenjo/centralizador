﻿

var Eventos = {

    LLenarComboNegocio: function () {
        $.ajax({
            url: urlListNegocio,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType:"json",
            success: function (response) {

                Funciones.LlenarCombo(response, "#cboEmpresa");
                $("#imgLogoNegocio").attr('src', $("#cboEmpresa option:selected").attr("data-image"));
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
    },

    Ingresar: function () {

        
        var request = new Object();
        
        request.NombreUsuario = $("#txtUsuario").val();
        request.Contraseña = $("#txtContraseña").val();
        request.IdNegocio = $("#cboEmpresa").val();

        $("#btnIngresar").prop("disabled", true);

        $.ajax({
            url: urlIngresarMenu,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ "request": request }),
            success: function (response) {
                if (response.IsSuccess) {
                    window.location.href = urlHomeSistema;
                }
                else {
                    window.location.href = urlLogin;
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    },

    
}

var Funciones = {

    LlenarCombo: function (data, control) {
        $(control).html("");
        var Opciones = '<option data-image="'+rutaImagenEmpresa+'" value="0">Seleccione empresa</option>';
        $.each(data, function (e, i) {
            Opciones = Opciones + '<option data-image="' +i.Img +'" value="' + i.Idnegocio + '">' + i.Nombre + '</option>';
        });
        $(control).html(Opciones);
    },

    LimpiarLogin: function () {
        $("#btnIngresar").prop("disabled", true);
    },
}

$(document).ready(function () {

    Eventos.LLenarComboNegocio();

    $("#btnIngresar").on("click", function () {
        var validar = true;
        if (!CMEN.validarCampo("#cboEmpresa")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#txtUsuario")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#txtContraseña")) {
            validar = false;
        }
        if (validar) {
            Funciones.LimpiarLogin();
            Eventos.Ingresar();
        }
    });
    $("#cboEmpresa").on("change", function () {
        $("#imgLogoNegocio").attr('src', $("#cboEmpresa option:selected").attr("data-image"));
    });

    $("#txtContraseña").keypress(function (e) {
        if (e.which == 13) {
            var validar = true;
            if (!CMEN.validarCampo("#cboEmpresa")) {
                validar = false;
            }
            if (!CMEN.validarCampo("#txtUsuario")) {
                validar = false;
            }
            if (!CMEN.validarCampo("#txtContraseña")) {
                validar = false;
            }
            if (validar) {
                Funciones.LimpiarLogin();
                Eventos.Ingresar();
            }
        }
    });

});