﻿
var Eventos = {

    SaveUnidadMedida: function () {
        var request = new Object();

        request.Nombre_medida = $("#txtUnidadMedida").val().trim().toUpperCase();
        request.Unidad_medida_id = $("#txtIdUnidadMedida").val();
        request.Estado = $("#cboEstado option:selected").text();
        request.Unidad_medidad_id_ref = $("#txtIdUnidadMedidaRef").val();
        if (request.Unidad_medida_id == "0") {
            request.EstadoAct = 0;
        } else {
            request.EstadoAct = 2;
        }

        $.ajax({
            url: urlSaveUnidadMedida,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ "request": request }),
            success: function (response) {

                if (response.IsSuccess) {
                    $('#grillaUnidadMedida').DataTable().ajax.reload();
                    $("#ModalAgregarUnidadMedida").modal("hide");
                } else {

                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    },
    GetUnidadMedida: function (IdUnidadMedida, NombreUnidadMedida, estado, IdUnidadMedidaRef) {

        Funciones.LimpiarAgregarUnidadMedida();
        $("#txtIdUnidadMedida").val(IdUnidadMedida);
        $("#txtIdUnidadMedidaRef").val(IdUnidadMedidaRef);
        $("#txtUnidadMedida").val(NombreUnidadMedida);
        if (estado == "ACTIVO") {
            $("#cboEstado").val(1);
        }
        else {
            $("#cboEstado").val(2);
        }
        $("#cboEstado").prop("disabled", false);
        $("#titleUnidadMedida").html("Editar Unidad Medida");
        $("#ModalAgregarUnidadMedida").modal("show");

    },


}

var Funciones = {
    RecargarDatosPorCambioEmpresa: function () {
        $("#grillaUnidadMedida").DataTable().ajax.reload();

        if ($("#cboLocal").val() != "0") {
            $("#botones").removeClass("hide");
        }
        else {
            $("#botones").addClass("hide");
        }
    },
    LimpiarAgregarUnidadMedida: function () {
        $("#txtIdUnidadMedida").val("0");
        $("#txtIdUnidadMedidaRef").val("0");
        CMEN.limpiarCampo("#txtUnidadMedida");
    },
}

var Grillas = {
    ListUnidadMedidaPaginado: function () {
        $('#grillaUnidadMedida').DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": false,
            "autoWidth": true,
            "destroy": true,
            "scrollY": "calc(100vh - 350px)",
            "scrollX": true,
            "language": CMEN.languageDatatable,
            "ajax": {
                "url": urlListUnidadMedidaPaginado,
                "type": 'POST',
                "datatype": "json",
                "contentType": 'application/json; charset=UTF-8',
                "dataSrc": "",
                "data": function (sSource) {
                    return JSON.stringify({ "page": sSource });
                },
            },
            "columns": [
                { "data": "Nombre_medida", "sClass": 'text-center' },
                { "data": "Estado", "sClass": 'text-center' },
                {
                    "data": function (row, type, set, meta) {
                        let Estado = '';
                        switch (row.EstadoAct) {
                            case 0:
                                Estado = '<div class="fondoPendiente">Pendiente</div>';
                                break;
                            case 1:
                                Estado = '<div class="fondoVigente">Actualizado</div>';
                                break;
                            case 2:
                                Estado = '<div class="fondoPendiente">Pendiente</div>';
                                break;
                        }
                        return Estado
                    }, "bSortable": false, "sClass": 'text-center'
                },
                {
                    "data": function (row, type, set, meta) {
                        var editar = '<a data-toggle="tooltip" data-placement="bottom" title="Editar" onclick="Eventos.GetUnidadMedida(\'' + row.Unidad_medida_id + '\',\'' + row.Nombre_medida + '\',\'' + row.Estado + '\',\'' + row.Unidad_medidad_id_ref + '\')" ><i class="fa fa-pencil-alt"></i></a>';
                        if (row.EstadoAct != 1) {
                            editar = '';
                        }
                        return editar;
                    }, "bSortable": false, "sClass": 'text-center'
                }
            ],
            "lengthMenu": [50, 100, 200],
            "pageLength": 50,
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip({ container: 'body' });
            }
        });
    },
}

$(document).ready(function () {

    Grillas.ListUnidadMedidaPaginado();

    if ($("#cboLocal").val() != "0") {
        $("#botones").removeClass("hide");
    }
    else {
        $("#botones").addClass("hide");
    }

    $("#btnRecargarUnidadMedida").on("click", function () {
        $('#grillaUnidadMedida').DataTable().ajax.reload();
    });

    $("#btnAgregarUnidadMedida").on("click", function () {
        $("#titleUnidadMedida").html("Agregar Unidad Medida");
        Funciones.LimpiarAgregarUnidadMedida();
        $("#cboEstado").val(1);
        $("#cboEstado").prop("disabled", true);
        $("#ModalAgregarUnidadMedida").modal("show");
    });

    $("#btnGuardarUnidadMedida").on("click", function () {
        var validar = true;
        if (!CMEN.validarCampo("#txtUnidadMedida")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#cboEstado")) {
            validar = false;
        }
        if (validar) {
            Eventos.SaveUnidadMedida();
        }
    });
});