﻿
var Eventos = {
    ListComboLinea: function () {
        $.ajax({
            url: urlListLinea,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                Funciones.LlenarComboLinea(response, "#cboLinea");
                Funciones.LlenarComboLinea(response, "#cboLineaAgregar");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
    },

    SaveCategoria: function () {
        var request = new Object();

        request.Nombrecategoria = $("#txtCategoria").val().trim().toUpperCase();
        request.Categoria_id = $("#txtIdCategoria").val();
        request.Estado = $("#cboEstado option:selected").text();
        request.Linea_id = $("#cboLineaAgregar").val();
        request.Categoria_id_ref = $("#txtIdCategoriaRef").val();
        if (request.Categoria_id == "0") {
            request.EstadoAct = 0;
        } else {
            request.EstadoAct = 2;
        }

        $.ajax({
            url: urlSaveCategoria,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ "request": request }),
            success: function (response) {

                if (response.IsSuccess) {
                    $('#grillaCategoria').DataTable().ajax.reload();
                    $("#ModalAgregarCategoria").modal("hide");
                } else {

                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    },
    GetCategoria: function (Categoria_id, Nombrecategoria, Estado, Linea_id, IdCategoriaRef) {

        $("#txtIdCategoria").val(Categoria_id);
        $("#txtIdCategoriaRef").val(IdCategoriaRef);
        $("#txtCategoria").val(Nombrecategoria);
        $("#cboLineaAgregar").val(Linea_id);
        if (Estado == "ACTIVO") {
            $("#cboEstado").val(1);
        }
        else {
            $("#cboEstado").val(2);
        }
        $("#cboEstado").prop("disabled", false);
        $("#titleCategoria").html("Editar Categoria");
        $("#ModalAgregarCategoria").modal("show");

    },
}

var Funciones = {
    RecargarDatosPorCambioEmpresa: function () {
        
        
        if ($("#cboLocal").val() != "0") {
            Eventos.ListComboLinea();
            $("#botones").removeClass("hide");
        }
        else {
            $("#botones").addClass("hide");
        }
    },

    LlenarComboLinea: function (data, control) {
        $(control).html("");
        var Opciones = '<option value="0">Seleccione linea</option>';
        $.each(data, function (e, i) {
            Opciones = Opciones + '<option value="' + i.Linea_id + '">' + i.Nombre + '</option>';
        });
        $(control).html(Opciones);
    },
    LimpiarAgregarCategoria: function () {
        $("#txtIdCategoria").val("0");
        $("#txtIdCategoriaRef").val("0");
        CMEN.limpiarCampo("#txtCategoria");
    },
}

var Grillas = {
    ListCategoriaPaginado: function () {
        $('#grillaCategoria').DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": false,
            "autoWidth": true,
            "destroy": true,
            "scrollY": "calc(100vh - 350px)",
            "scrollX": true,
            "language": CMEN.languageDatatable,
            "ajax": {
                "url": urlListCategoriaPaginado,
                "type": 'POST',
                "datatype": "json",
                "contentType": 'application/json; charset=UTF-8',
                "dataSrc": "",
                "data": function (sSource) {
                    var IdLinea = $("#cboLinea").val();
                    return JSON.stringify({ "page": sSource, "IdLinea" : IdLinea });
                },
            },
            "columns": [
                { "data": "Nombrecategoria", "sClass": 'text-left' },
                { "data": "Estado", "sClass": 'text-center' },
                {
                    "data": function (row, type, set, meta) {
                        let Estado = '';
                        switch (row.EstadoAct) {
                            case 0:
                                Estado = '<div class="fondoPendiente">Pendiente</div>';
                                break;
                            case 1:
                                Estado = '<div class="fondoVigente">Actualizado</div>';
                                break;
                            case 2:
                                Estado = '<div class="fondoPendiente">Pendiente</div>';
                                break;
                        }
                        return Estado
                    }, "bSortable": false, "sClass": 'text-center'
                },
                {
                    "data": function (row, type, set, meta) {
                        var editar = '<a data-toggle="tooltip" data-placement="bottom" title="Editar" onclick="Eventos.GetCategoria(\'' + row.Categoria_id + '\',\'' + row.Nombrecategoria + '\',\'' + row.Estado + '\',\'' + row.Linea_id + '\',\''+ row.Categoria_id_ref +'\')" ><i class="fa fa-pencil-alt"></i></a>';
                        if (row.EstadoAct != 1) {
                            editar = '';
                        }
                        return editar;
                    }, "bSortable": false, "sClass": 'text-center'
                }
            ],
            "lengthMenu": [50, 100, 200],
            "pageLength": 50,
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip({ container: 'body' });
            }
        });
    },
}

$(document).ready(function () {

    Grillas.ListCategoriaPaginado();
    if ($("#cboLocal").val() != "0") {
        Eventos.ListComboLinea();
        $("#botones").removeClass("hide");
    }
    else {
        $("#botones").addClass("hide");
    }

    $("#cboLinea").on("change", function () {
        $("#grillaCategoria").DataTable().ajax.reload();
    });

    $("#btnAgregarCategoria").on("click", function () {
        $("#titleCategoria").html("Agregar Categoria");
        Funciones.LimpiarAgregarCategoria();
        $("#cboEstado").val(1);
        $("#cboEstado").prop("disabled", true);
        $("#ModalAgregarCategoria").modal("show");
    });
    $("#btnRecargarCategoria").on("click", function () {
        $("#grillaCategoria").DataTable().ajax.reload();
    });

    $("#btnGuardarCategoria").on("click", function () {
        var validar = true;
        if (!CMEN.validarCampo("#txtCategoria")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#cboLineaAgregar")) {
            validar = false;
        }
        if (!CMEN.validarCampo("#cboEstado")) {
            validar = false;
        }
        if (validar) {
            Eventos.SaveCategoria();
        }
    });

});