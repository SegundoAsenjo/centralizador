﻿

function strPad(i, l, s) {
    var o = i.toString();
    if (!s) { s = '0'; }
    while (o.length < l) {
        o = s + o;
    }
    return o;
}

function parseJsonDate(jsonDate) {
    if (jsonDate == null)
        return '';
    var value = new Date(parseInt(jsonDate.substr(6)));
    var ret = strPad(value.getDate(), 2) + "/" + strPad(value.getMonth() + 1, 2) + "/" + value.getFullYear();
    return ret;
}

var CMEN = {
    languageDatatable: {
        "decimal": ".",
        "lengthMenu": "Mostrar _MENU_ registros por página",
        "zeroRecords": "No se encontraron registros.",
        "info": "Pág. _PAGE_ de _PAGES_ (_TOTAL_ registros)",
        "infoEmpty": "No hay registros disponibles.",
        "search": "Buscar",
        "thousands": ",",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "emptyTable": "No hay registros disponibles",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "infoFiltered": "(filtrados de _MAX_ registros totales)"
    },
}