﻿

var Eventos = {

    LLenarComboLocal: function () {
        $.ajax({
            url: urlListLocales,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                Funciones.LlenarCombo(response, "#cboLocal");
                if (response.length == 1) {
                    $("#cboLocal").val(1).trigger("change");
                    $("#cboLocal").prop("disabled", true);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
    },

    VentasXCierreCaja: function (IdAperturaCierreCaja) {
        $("#txtIdAperturaCierreCaja").val(IdAperturaCierreCaja);
        $("#grillaVentasXCierreCaja").DataTable().ajax.reload();

        $("#modalVentasXCierreCaja").modal("show");

    },

    DetalleVentas: function (IdVenta) {
        $("#txtIdVenta").val(IdVenta);
        $("#grillaDetalleVenta").DataTable().ajax.reload();

        $("#modalDetalleVenta").modal("show");

    },

}

var Funciones = {

    LlenarCombo: function (data, control) {
        $(control).html("");
        var Opciones = '<option value="0">Seleccione local</option>';
        $.each(data, function (e, i) {
            Opciones = Opciones + '<option value="' + i.Idlocal + '">' + i.NombreLocal + '</option>';
        });
        $(control).html(Opciones);
    },

}

var Grillas = {
    ListCierreCajaPaginado: function () {
        $('#grillaCierreCaja').DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": false,
            "autoWidth": true,
            "destroy": true,
            "scrollY": "calc(100vh - 350px)",
            "scrollX": true,
            "language": CMEN.languageDatatable,
            "ajax": {
                "url": urlListCierreCajaXLocalPaginado,
                "type": 'POST',
                "datatype": "json",
                "contentType": 'application/json; charset=UTF-8',
                "dataSrc": "",
                "data": function (sSource) {
                    var IdLocal = $("#cboLocal").val();
                    return JSON.stringify({ "page": sSource, "IdLocal" : IdLocal });
                },
            },
            "columns": [
                { "data": "FechaCaja", "sClass": 'text-center' },
                { "data": "Horaini", "sClass": 'text-center' },
                { "data": "Montoinicial", "sClass": 'text-center' },
                { "data": "Totalefect", "sClass": 'text-center' },
                { "data": "Totalcompras", "sClass": 'text-center' },
                { "data": "Totalpagos", "sClass": 'text-center' },
                {
                    "data": function (row, type, set, meta) {
                        var detalle = '<a data-toggle="tooltip" data-placement="bottom" title="Detalle" onclick="Eventos.VentasXCierreCaja(' + row.IdAperturacierre +')" ><i class="fa fa-file-alt"></i></a>';
                        return detalle;
                    }, "bSortable": false, "sClass": 'text-center'
                }
            ],
            "lengthMenu": [50, 100, 200],
            "pageLength": 50,
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip({ container: 'body' });
            }
        });
    },

    ListVentaXCierreCajaPaginado: function () {
        $('#grillaVentasXCierreCaja').DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": false,
            "autoWidth": false,
            "destroy": true,
            "language": CMEN.languageDatatable,
            "ajax": {
                "url": urlListVentaXCierreCajaPaginado,
                "type": 'POST',
                "datatype": "json",
                "contentType": 'application/json; charset=UTF-8',
                "dataSrc": "",
                "data": function (sSource) {
                    var IdAperturaCierre = $("#txtIdAperturaCierreCaja").val();
                    return JSON.stringify({ "page": sSource, "IdAperturaCierre": IdAperturaCierre });
                },
            },
            "columns": [

                { "data": "Tipo_documento", "sClass": 'text-center'},
                { "data": "NroSerie", "sClass": 'text-center'},
                { "data": "Cliente", "sClass": 'text-center' },
                { "data": "Personal", "sClass": 'text-center' },
                { "data": "Total", "sClass": 'text-center' },
                { "data": "Razon_social", "sClass": 'text-center' },
                {
                    "data": function (row, type, set, meta) {
                        var detalle = '<a data-toggle="tooltip" data-placement="bottom" title="Detalle" onclick="Eventos.DetalleVentas(' + row.Pedido_ID + ')" ><i class="fa fa-file-alt"></i></a>';
                        return detalle;
                    }, "bSortable": false, "sClass": 'text-center'
                }
            ],
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip({ container: 'body' });
            }
        });
    },

    ListDetalleVentaPaginado: function () {
        $('#grillaDetalleVenta').DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": false,
            "autoWidth": false,
            "destroy": true,
            "language": CMEN.languageDatatable,
            "ajax": {
                "url": urlListDetalleVentaPaginado,
                "type": 'POST',
                "datatype": "json",
                "contentType": 'application/json; charset=UTF-8',
                "dataSrc": "",
                "data": function (sSource) {
                    var IdVenta = $("#txtIdVenta").val();
                    return JSON.stringify({ "page": sSource, "IdVenta": IdVenta });
                },
            },
            "columns": [

                { "data": "DescripcionProd", "sClass": 'text-center' },
                { "data": "Cantidad", "sClass": 'text-center' },
                { "data": "PrecioUnitario", "sClass": 'text-center' },
                { "data": "Igvd", "sClass": 'text-center' },
                { "data": "Subtotald", "sClass": 'text-center' },
            ],
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip({ container: 'body' });
            }
        });
    },

    
}

$(document).ready(function () {

    Eventos.LLenarComboLocal();
    Grillas.ListCierreCajaPaginado();
    Grillas.ListVentaXCierreCajaPaginado();
    Grillas.ListDetalleVentaPaginado();
    $("#cboLocal").on("change", function () {
        $("#grillaCierreCaja").DataTable().ajax.reload();
    });

    $("#modalDetalleVenta").on("hidden.bs.modal", function () {
        $("body").addClass("modal-open");
    });

});